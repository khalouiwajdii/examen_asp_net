## :pencil: Project Description
This is my instagram Application for hexagone student.

# Setup to run it on you Computer
You need the .NET Core 3.1 SDK (Download: https://dotnet.microsoft.com/download) and the Microsoft Sql Server (Download: https://www.microsoft.com/en-au/sql-server/sql-server-downloads *Free Developer Version)

- When you Run the project it will generate a sql database without password just login with your server :localhost
- The default connection in ./appsettings.json 


# Features
- Login
- Register
- Comments
- Post images
- Like
- about
- follow
  
 # Sources :
 - https://github.com/kcrnac/ng-instagram/tree/master/backend

# capture
 
  <img src="./capture/Login.PNG" width="700">

  <hr />
  <img src="./capture/Register.PNG" width="700">

  <hr />

  <img src="./capture/Profil.PNG" width="700">
  
  <hr />

  <img src="./capture/Add imgg.PNG" width="700">
  
  <hr />


  <img src="./capture/Add img.PNG" width="700">
  
  <hr />
 <img src="./capture/Comment.PNG " width="700">
  
  <hr />

 <img src="./capture/follower.PNG " width="700">
  
  <hr />
<img src="./capture/follower2.PNG " width="700">
  
  <hr />
  <img src="./capture/Home.PNG " width="700">
  
  <hr />
